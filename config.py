SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = False

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://userid:userpass@localhost:port/db_name?charset=utf8mb4'

WEB_SCRAPPING_BASE_URL = "https://websites.co.in/sitemap"
