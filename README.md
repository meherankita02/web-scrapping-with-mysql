### Coding Guidelines ###

* Keep your code readable
* Follow pep8 conventions, including but not limited to the following:
    1. 80 character lines limit
    2. double newlines between classes, and functions
    3. single newlines between methods of classes
* Futher reading here: https://docs.google.com/document/d/1maGMJBjeiC-bJ5r5hRKen85a8LGujRKcCH8-J2fBNGw/edit


#### Python Dependencies and application run ####

1. install virtualenv: https://virtualenv.pypa.io/en/latest/installation/
2. create virtualenv with python3 (your system might have python3 as python):
    ```
    python3 -m pip install --user virtualenv
    python3 -m venv env

    ```
3. enable virtualenv:
    ```
    . venv/bin/activate or source env/bin/activate
    ```
4. install python dependencies:
    ```
    pip install -r requirements.txt
    ```
5. run the application:
    ```
    python run.py
    ```
6. Leaving the virtual environment¶
    ```
    deactivate
    ```

#### Install Notes ####

    ```
    pip install flask_sqlalchemy
    pip install flask_marshmallow
    pip install Flask-Migrate
    pip install cryptography
    pip install PyMySQL
    ```

#### Install Notes ####


#### DB Setup ####

1. install mysql using following command: brew install mysql
2. start mysql service: brew services start mysql
3. stop mysql service: brew services stop mysql
4. start service: mysql.server start
5. used for configuration settings: mysql_secure_installation
6. start mysql using default user: mysql -u root -p
7. create new user if you want: CREATE USER 'idnewuser'@'localhost' IDENTIFIED BY 'passnewuser';
8. grant access to new user: GRANT ALL PRIVILEGES ON * . * TO 'idnewuser'@'localhost';
9. quit
10. start mysql with new user: mysql -u idnewuser -p
11. create database: CREATE DATABASE new_db;
12. check created database: SHOW DATABASES;
14. access/use created database: USE new_db;
15. SELECT * FROM tablename;
16. quit












