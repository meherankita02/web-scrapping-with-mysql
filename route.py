from flask import Blueprint
from flask_restful import Api
from app.resources.web_api import WebScrapAPI

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

api.add_resource(WebScrapAPI, '/')
