"""
Setup the app
"""
from flask import Flask

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# Define the WSGI application object
flaskapp = Flask(__name__)
db = SQLAlchemy()
ma = Marshmallow()

# Define the WSGI application object
flaskapp = Flask(__name__)


def create_app(config):
    """
    Function that creates the app.
    """

    # global app object, configure it!
    flaskapp.config.from_object(config)

    configure_extensions(flaskapp)
    configure_apis(flaskapp)

    with flaskapp.app_context():
        db.create_all()

    return flaskapp


def configure_extensions(app, socket=False, main=True):
    """
    Configures the extensions being used
    """

    db.init_app(app)  # flask-sqlalchemy
    ma.init_app(app)  # flask-marshmallow

    return


def configure_apis(app):
    """
    Configures the apis exposed
    """

    from route import api_bp as web_api_bp
    app.register_blueprint(web_api_bp, url_prefix='/api')

    return app
