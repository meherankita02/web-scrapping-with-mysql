import requests

from flask_restful import Resource, current_app
from flask import request
from bs4 import BeautifulSoup

from app import db
from app.resources.schemas import WebsiteSchema
from app.resources.models import Website


class WebScrapAPI(Resource):
    def get(self):
        url = current_app.config['WEB_SCRAPPING_BASE_URL']
        print(url)

        if 'page' in request.args:
            page = request.args['page']
            url = url + '?page=' + page

        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        table = soup.find('table', {'class': 'table'})

        web_data = []
        for table_row in table.findAll('tr'):
            columns = table_row.findAll('td')
            inner_data = []
            for column in columns:
                inner_data.append(column.text.replace('\n', ' ').strip())
            web_data.append(inner_data)

        for data in web_data:
            if data:
                json_data = {
                    'business_name': data[0],
                    'category': data[1],
                    'city': data[2]
                }

                get_data = Website.query.filter(
                    Website.business_name == json_data[
                        'business_name']).first()
                if get_data is None:
                    website_data = WebsiteSchema().load(json_data)

                    db.session.add(website_data)
                    db.session.commit()

        return "Data Saved Successfully"
