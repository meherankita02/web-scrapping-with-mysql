import datetime

from app import db


class Website(db.Model):
    __tablename__ = 'websites'

    id = db.Column(db.Integer, primary_key=True)
    business_name = db.Column(db.String(255))
    category = db.Column(db.String(255))
    city = db.Column(db.String(255))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    def __init__(self, business_name, category, city):
        self.business_name = business_name
        self.category = category
        self.city = city
