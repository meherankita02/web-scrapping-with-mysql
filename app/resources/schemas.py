from marshmallow import EXCLUDE

from app import ma
from app.resources.models import Website


class WebsiteSchema(ma.ModelSchema):
    class Meta:
        model = Website
        unknown = EXCLUDE
