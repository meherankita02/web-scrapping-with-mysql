import os

from app import create_app

config_module = os.environ.get('config_module', 'config')

# create the app
flaskapp = create_app(config_module)

if __name__ == '__main__':
    flaskapp.run(debug=flaskapp.debug)
